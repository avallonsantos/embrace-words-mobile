// Array above contains all meta tags that will be rendered in final LP code
// To use it just create a new object inside array with a couple of attributes. Generally name and content

module.exports = [
    {
        name: 'og:url',
        content: 'Palavras que Abraçam | ClickBus'
    },

    {
        name: 'description',
        content: 'Palavras que Abraçam | ClickBus'
    },
]