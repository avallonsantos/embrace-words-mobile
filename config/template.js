const path = require('path')
// Importing meta tags for this landing page
const Metas = require('./metas');

// Exporting literal object with configuration of HTML Webpack Plugin

module.exports = {
    links: [],
    scripts: [],
    title: 'Palavras que Abraçam | ClickBus',
    mobile: true,
    lang: 'pt_BR',
    template: path.resolve(__dirname, './../src/static/index.html'),
    meta: Metas,
    renderPlaceholders: false
};